package com.competitions.demo.auth.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NonNull;

@Data
@ApiModel(description = "Registration request")
public class RegisterDTO {
    @NonNull
    private String username;
    @NonNull
    private String password;
    @NonNull
    private String firstName;
    @NonNull
    private String lastName;
}
