package com.competitions.demo.auth.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NonNull;

@Data
@ApiModel(description = "Login response")
public class AuthResponse {
    @NonNull
    private String token;
}
