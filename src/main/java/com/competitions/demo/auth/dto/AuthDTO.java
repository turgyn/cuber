package com.competitions.demo.auth.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NonNull;

@Data
@ApiModel(description = "Login request")
public class AuthDTO {
    @NonNull
    private String username;
    @NonNull
    private String password;
}
