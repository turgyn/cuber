package com.competitions.demo.auth.jwt;

import io.jsonwebtoken.*;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@Log
public class JwtProvider {

    @Value("@(jwt.secret)")
    private String jwtSecret;

    public String generateToken(String login) {
        Date now = new Date();
        long validityInMilliseconds = 4 * 60 * 60 * 1000;   // 4 hours
        Date date = new Date(now.getTime() + validityInMilliseconds);
        return Jwts.builder()
                .setSubject(login)
                .setExpiration(date)
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    public boolean validateToken(String token) {
        try {
            Jws<Claims> claims = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token);
            boolean expired = claims.getBody().getExpiration().before(new Date());
            if (expired) {
                log.info("Token expired");
                return false;
            }
            log.info("Valid token");
            return true;
        } catch (Exception e) {
            log.severe("Invalid token");
        }
        return false;
    }

    public String getLoginFromToken(String token) {
        Claims claims = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();
        return claims.getSubject();
    }
}