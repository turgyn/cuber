package com.competitions.demo.auth;

import com.competitions.demo.auth.dto.AuthDTO;
import com.competitions.demo.auth.dto.RegisterDTO;
import com.competitions.demo.auth.jwt.JwtProvider;
import com.competitions.demo.app.entities.User;
import com.competitions.demo.app.services.UserService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.java.Log;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;

@RestController
@Log
@RequestMapping("/api")
public class AuthController {

    private final UserService userService;

    private final JwtProvider jwtProvider;

    public AuthController(UserService userService, JwtProvider jwtProvider) {
        this.userService = userService;
        this.jwtProvider = jwtProvider;
    }

    @PostMapping("/register")
    @ApiOperation(value = "Regitration")
    public ResponseEntity<Object> registerUser(@RequestBody @Valid RegisterDTO request) {
        User user = new User(
                request.getUsername(),
                request.getPassword()
        );
        user.setFirstName(request.getFirstName());
        user.setLastName(request.getLastName());
        userService.create(user);
        return ResponseEntity.ok("New user created successfully");
    }

    @PostMapping("/auth")
    @ApiOperation(value = "login")
    public ResponseEntity<Object> auth(@RequestBody @Valid AuthDTO request) {
        User user = userService.findByUsernameAndPassword(request.getUsername(), request.getPassword());
        if (user != null) {
            String token = jwtProvider.generateToken(user.getUsername());
            HashMap<String, String> response = new HashMap<>();
            response.put("token", token);
            return ResponseEntity.ok(response);
        }
        return ResponseEntity.badRequest().body("Wrong username or password");
    }
}
