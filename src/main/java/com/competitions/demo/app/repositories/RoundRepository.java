package com.competitions.demo.app.repositories;

import com.competitions.demo.app.entities.Record;
import com.competitions.demo.app.entities.Round;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoundRepository extends JpaRepository<Round, Long> {
    Round findByRecord_Competition_IdAndRecord_User_UsernameAndRoundNum(String comp_id, String username, Integer round_num);

    List<Round> findAllByRecord(Record record);
}
