package com.competitions.demo.app.repositories;

import com.competitions.demo.app.entities.Record;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecordRepository extends JpaRepository<Record, Long> {
    List<Record> findAllByCompetition_Id(String id);

    Record findByCompetition_IdAndUser_Username(String comp_id, String username);

    List<Record> findAllByUser_Username(String username);

    boolean existsByCompetition_IdAndUser_Username(String competitionId, String username);
}
