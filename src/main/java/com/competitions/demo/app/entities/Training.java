package com.competitions.demo.app.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Entity
@Table(name = "trainings")
@Data
@ApiModel("Training rounds, not calculated for user ratings")
public class Training {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @ManyToOne(targetEntity = User.class)
    @JsonIgnore
    private User user;

    @Min(0)
    @Max(30)

    @ApiModelProperty(value = "Max amount of rotations, that user solved")
    private Integer level;

    @Min(0)
    private Double time;
}
