package com.competitions.demo.app.entities;

public enum Role {
    ROLE_USER, ROLE_ADMIN
}
