package com.competitions.demo.app.entities;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "competitions")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "Competitions organized by administration")
public class Competition {
    @Id
    @NotNull
    private String id;

    @NotNull
    private String name;

    private String information;
}
