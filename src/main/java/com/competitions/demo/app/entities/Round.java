package com.competitions.demo.app.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Entity
@Table(name = "rounds")
@Data
@ApiModel(description = "Each participant has 5 rounds in one competition")
public class Round {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JsonIgnore
    private Record record;

    @Min(1)
    @Max(5)
    @Column(name = "round_num")
    private Integer roundNum;

    private Boolean taken = false;

    @ApiModelProperty(value = "Disassemble algorithm for cube")
    private String scramble;

    @Min(0)
    private Double time;
}
