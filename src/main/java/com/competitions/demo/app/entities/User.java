package com.competitions.demo.app.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

@Entity
@Table(name = "users", uniqueConstraints = {@UniqueConstraint(columnNames = "username")})
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Personal information and stats")
public class User implements Comparable<User> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @Size(min = 4, max = 30, message = "Username size should be [4-30]")
    private String username;

    @JsonIgnore
    private String password;

    @Enumerated(EnumType.ORDINAL)
    @JsonIgnore
    private Role role = Role.ROLE_USER;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "best_single")
    @Min(0)
    private Double bestSingle;

    @Column(name = "best_avg")
    @Min(0)
    private Double bestAvg;

    @Column(name = "train_level")
    @Min(0)
    @Max(30)
    @ApiModelProperty(value = "max amount of rotations in training room, that user solved")
    private Integer trainLevel = 0;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    public int compareTo(User o) {
        if (bestSingle == null) return 1;
        if (o.bestSingle == null) return -1;
        return Double.compare(bestSingle, o.getBestSingle());
    }
}

