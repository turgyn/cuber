package com.competitions.demo.app.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "records")
@Data
@ApiModel(description = "Results of User on each competition")
public class Record implements Comparable<Record> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @NotNull
    private User user;

    @ManyToOne(cascade = CascadeType.ALL)
    @NotNull
    @JsonIgnore
    private Competition competition;

    @ApiModelProperty(value = "amount of completed rounds out of 5")
    private Integer rounds_complete = 0;

    @Min(0)
    @ApiModelProperty(value = "Smallest time, throw the 5 rounds")
    private Double best_time;

    @Min(0)
    private Double worst_time;

    @Min(0)
    @ApiModelProperty(value = "Resultant time. Arithmetic average of 3 rounds w/o best & words")
    private Double average;


    @Override
    public int compareTo(Record o) {
        if (best_time == null) return 1;
        if (o.best_time == null) return -1;
        return Double.compare(best_time, o.best_time);
    }
}
