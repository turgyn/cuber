package com.competitions.demo.app.controllers;

import com.competitions.demo.app.dto.TrainResultDTO;
import com.competitions.demo.app.entities.Training;
import com.competitions.demo.app.services.TrainingService;
import com.competitions.demo.util.ScrambleGenerator;
import io.swagger.annotations.ApiOperation;
import org.apache.coyote.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class TrainingController {

    private final TrainingService trainingService;

    public TrainingController(TrainingService trainingService) {
        this.trainingService = trainingService;
    }

    @GetMapping("/training")
    @ApiOperation(value = "Get disassemble algorithm(scramble) with {level} rotations")
    public String getTraining(@PathParam(value = "level") Integer level) {
        return trainingService.getScrambleByLevel(level);
    }

    @PostMapping("/training")
    @ApiOperation(value = "Set training results")
    public ResponseEntity<Object> setTrainResult(@RequestBody TrainResultDTO trainResultDTO) {
        String message = "Your previous attempt was faster";
        if (trainingService.setTrainResults(trainResultDTO)) {
            message = "You have new record";
        }
        return ResponseEntity.ok(message);
    }

    @GetMapping("/users/{username}/trainings")
    @ApiOperation("Get training profile of user")
    public List<Training> getUserTrainings(@PathVariable String username) {
        return trainingService.getUserTrainings(username);
    }

}
