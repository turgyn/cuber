package com.competitions.demo.app.controllers;

import com.competitions.demo.app.dto.RecordDTO;
import com.competitions.demo.app.entities.Record;
import com.competitions.demo.app.services.RecordService;
import com.competitions.demo.app.services.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.java.Log;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Log
@RequestMapping("/api/v1")
public class RecordController {
    private final RecordService recordService;
    private final UserService userService;

    public RecordController(RecordService recordService, UserService userService) {
        this.recordService = recordService;
        this.userService = userService;
    }

    @GetMapping("/competitions/{comp_id}")
    @ApiOperation(value = "Get all participants and results of competition")
    public List<Record> getCompetitionResults(@PathVariable String comp_id) {
        return recordService.findAllByCompetitionId(comp_id);
    }

    @PostMapping("/competitions/{comp_id}/register")
    @ApiOperation(value = "Register for competition")
    public ResponseEntity<Object> registerForCompetition(@PathVariable String comp_id) {
        recordService.registerForCompetition(comp_id, userService.getTokenUserDetails().getUsername());
        return ResponseEntity.ok("Registration complete successfully");
    }

    @GetMapping("/competitions/{comp_id}/{username}")
    @ApiOperation(value = "Get Competition results of specific user")
    public RecordDTO getCompetitionUserResults(@PathVariable String comp_id, @PathVariable String username) {
        return new RecordDTO(recordService.findByCompetitionAndUser(comp_id, username));
    }

    @GetMapping("/users/{username}/records")
    @ApiOperation(value = "Get all participations of user")
    public List<RecordDTO> getUserRecords(@PathVariable String username) {
        return recordService.findAllByUsername(username);
    }
}
