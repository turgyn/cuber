package com.competitions.demo.app.controllers;

import com.competitions.demo.app.entities.User;
import com.competitions.demo.app.services.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("")
    @ApiOperation(value = "Get list of all Users")
    public List<User> getUsers() {
        return userService.findAll();
    }

    @GetMapping("/{username}")
    @ApiOperation(value = "Get specific user")
    public User getUser(@PathVariable String username) {
        return userService.findByUsername(username);
    }
}
