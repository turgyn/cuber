package com.competitions.demo.app.controllers;

import com.competitions.demo.app.dto.RoundResultsDTO;
import com.competitions.demo.app.entities.Round;
import com.competitions.demo.app.services.RecordService;
import com.competitions.demo.app.services.RoundService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.java.Log;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1")
@Log
public class RoundController {
    private final RoundService roundService;
    private final RecordService recordService;

    public RoundController(RoundService roundService, RecordService recordService) {
        this.roundService = roundService;
        this.recordService = recordService;
    }

    @GetMapping("/competitions/{comp_id}/{username}/{round_num}")
    @ApiOperation(value = "Get scramble and time if finished")
    public Round getRoundResults(
            @PathVariable String comp_id,
            @PathVariable String username,
            @PathVariable Integer round_num) {
        return roundService.findByCompetitionUserRound(comp_id, username, round_num);
    }

    @PostMapping("/competitions/{comp_id}/{username}/{round_num}")
    @ApiOperation(value = "Set round results")
    public ResponseEntity<Object> setRoundResults(
            @PathVariable String comp_id,
            @PathVariable String username,
            @PathVariable Integer round_num,
            @RequestBody RoundResultsDTO roundResultsDTO){
        log.info(roundResultsDTO.toString());
        recordService.setRound(comp_id, username, round_num, roundResultsDTO);
        log.info("exited from service");
        return ResponseEntity.ok("Round results posted");
    }
}
