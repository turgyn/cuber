package com.competitions.demo.app.controllers;

import com.competitions.demo.app.entities.Competition;
import com.competitions.demo.app.services.CompetitionService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestControllerAdvice
@RequestMapping("/api/v1/competitions")
public class CompetitionController {

    private final CompetitionService competitionService;

    public CompetitionController(CompetitionService competitionService) {
        this.competitionService = competitionService;
    }

    @GetMapping
    @ApiOperation(value = "Find all competitions")
    public List<Competition> getCompetitions() {
        return competitionService.findAll();
    }

    @PostMapping
    @ApiOperation(value = "Create new Competition", consumes = "Competition")
    public ResponseEntity<Object> createCompetition(@RequestBody @Valid Competition competition) {
        competitionService.create(competition);
        return ResponseEntity.ok("Competition created successfully");
    }
}
