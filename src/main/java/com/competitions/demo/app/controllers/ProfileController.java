package com.competitions.demo.app.controllers;

import com.competitions.demo.app.entities.User;
import com.competitions.demo.app.services.UserService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.java.Log;
import org.springframework.web.bind.annotation.*;

@RestController
@Log
@RequestMapping("api/v1/profile")
public class ProfileController {

    private final UserService userService;

    public ProfileController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    @ApiOperation(value = "get User information and stats")
    public User getProfile() {
        return userService.getTokenUser();
    }

    @PostMapping
    @ApiOperation(value = "Update firstName/ lastName/ password")
    public User setProfile(@RequestBody User user) {
        return userService.updateProfile(user);
    }
}
