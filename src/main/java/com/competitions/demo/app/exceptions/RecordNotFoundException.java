package com.competitions.demo.app.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class RecordNotFoundException extends RuntimeException {
    public RecordNotFoundException(String comp_id, String username) {
        super(String.format("Record with competition: %s, user: %s not found", comp_id, username));
    }
}
