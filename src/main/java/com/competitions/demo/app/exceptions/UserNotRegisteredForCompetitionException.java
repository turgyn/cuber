package com.competitions.demo.app.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class UserNotRegisteredForCompetitionException extends RuntimeException {
    public UserNotRegisteredForCompetitionException(String username, String comp_id) {
        super(username + " not registered for " + comp_id);
    }
}
