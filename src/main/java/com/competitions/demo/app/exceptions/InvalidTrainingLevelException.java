package com.competitions.demo.app.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidTrainingLevelException extends RuntimeException {
    public InvalidTrainingLevelException(String msg) {
        super(msg);
    }
}
