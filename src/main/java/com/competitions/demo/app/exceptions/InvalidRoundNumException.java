package com.competitions.demo.app.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class InvalidRoundNumException extends RuntimeException {
    public InvalidRoundNumException(String msg) {
        super(msg);
    }
}
