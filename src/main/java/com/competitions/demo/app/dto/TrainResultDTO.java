package com.competitions.demo.app.dto;

import lombok.Data;
import lombok.NonNull;

@Data
public class TrainResultDTO {
    @NonNull
    int level;
    @NonNull
    Double time;
}
