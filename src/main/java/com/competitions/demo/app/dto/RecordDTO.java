package com.competitions.demo.app.dto;

import com.competitions.demo.app.entities.Record;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "competition_id instead of whole competition, username instead of user")
public class RecordDTO {
    private Long id;
    private String competition_id;
    private String username;
    private Integer rounds_complete = 0;
    @Min(0)
    private Double best_time;
    @Min(0)
    private Double worst_time;
    @Min(0)
    private Double average;

    public RecordDTO(Record record) {
        this.id = record.getId();
        this.competition_id = record.getCompetition().getId();
        this.rounds_complete = record.getRounds_complete();
        this.username = record.getUser().getUsername();
        this.best_time = record.getBest_time();
        this.worst_time = record.getWorst_time();
        this.average = record.getAverage();
    }
}
