package com.competitions.demo.app.services;

import com.competitions.demo.app.dto.TrainResultDTO;
import com.competitions.demo.app.entities.Training;

import java.util.List;

public interface TrainingService {
    String getScrambleByLevel(int level);

    boolean setTrainResults(TrainResultDTO trainResultDTO);

    List<Training> getUserTrainings(String username);
}
