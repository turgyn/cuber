package com.competitions.demo.app.services;

import com.competitions.demo.app.entities.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

public interface UserService {
    List<User> findAll();

    User findByUsername(String username);

    User findByUsernameAndPassword(String username, String password);

    void create(User user);

    boolean isExists(String username);

    User update(User user);

    User updateProfile(User user);

    User getTokenUser();

    UserDetails getTokenUserDetails();
}
