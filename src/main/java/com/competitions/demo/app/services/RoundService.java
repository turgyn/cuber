package com.competitions.demo.app.services;

import com.competitions.demo.app.dto.RoundResultsDTO;
import com.competitions.demo.app.entities.Record;
import com.competitions.demo.app.entities.Round;

import java.util.List;

public interface RoundService {

    Round findByCompetitionUserRound(String comp_id, String username, Integer round_num);

    List<Round> findByRecord(Record record);

    void create(Round round);

    void update(Round round);
}
