package com.competitions.demo.app.services.impl;

import com.competitions.demo.app.entities.User;
import com.competitions.demo.app.repositories.UserRepository;
import com.competitions.demo.app.services.UserService;
import lombok.extern.java.Log;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
@Log
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository userRepository, @Lazy PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public List<User> findAll() {
        List<User> users =  userRepository.findAll();
        Collections.sort(users);
        return users;
    }

    @Override
    public User findByUsername(String username) {
        User user = userRepository.findUserByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username + " not found");
        }
        return user;
    }

    @Override
    public User findByUsernameAndPassword(String username, String password) {
        User user = findByUsername(username);
        if (passwordEncoder.matches(password, user.getPassword())) {
            return user;
        }
        throw new UsernameNotFoundException("Wrong username or password");
    }

    @Override
    public void create(User user) {
        if (!isExists(user.getUsername())) {             // Throws UsernameNotFoundException
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            userRepository.save(user);
        }
    }

    @Override
    public User update(User user) {
        userRepository.save(user);
        return findByUsername(user.getUsername());
    }

    @Override
    public User updateProfile(User new_user) {
        String username = getTokenUserDetails().getUsername();
        User user = findByUsername(username);
        if (new_user.getPassword() != null) {
            user.setPassword(passwordEncoder.encode(new_user.getPassword()));
        }
        if (new_user.getFirstName() != null) {
            user.setFirstName(new_user.getFirstName());
        }
        if (new_user.getLastName() != null) {
            user.setLastName(new_user.getLastName());
        }
        log.info(username + "updated profile: " + user.toString());
        return update(user);
    }

    @Override
    public boolean isExists(String username) {
        return userRepository.existsByUsername(username);
    }

    @Override
    public User getTokenUser() {
        return findByUsername(getTokenUserDetails().getUsername());
    }

    @Override
    public UserDetails getTokenUserDetails() {
        Object o = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        log.info(o.toString());
        return (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
