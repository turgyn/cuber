package com.competitions.demo.app.services;

import com.competitions.demo.app.entities.Competition;
import com.competitions.demo.app.exceptions.CompetitionNotFoundException;

import java.util.List;

public interface CompetitionService {

    List<Competition> findAll();

    Competition findById(String id) throws CompetitionNotFoundException;

    boolean isExists(String id);

    void create(Competition competition);
}
