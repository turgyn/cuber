package com.competitions.demo.app.services;

import com.competitions.demo.app.dto.RecordDTO;
import com.competitions.demo.app.dto.RoundResultsDTO;
import com.competitions.demo.app.entities.*;
import com.competitions.demo.app.entities.Record;
import com.competitions.demo.app.exceptions.CompetitionNotFoundException;

import java.util.List;

public interface RecordService {
    void create(Competition competition, User user);

    List<Record> findAllByCompetitionId(String id);

    Record findByCompetitionAndUser(String comp_id, String username) throws CompetitionNotFoundException;

    void registerForCompetition(String comp_id, String username) throws CompetitionNotFoundException;

    List<RecordDTO> findAllByUsername(String username);

    boolean isExists(String comp_id, String username);

    void setRound(String comp_id, String username, int round_num, RoundResultsDTO roundResultsDTO);
}
