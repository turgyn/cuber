package com.competitions.demo.app.services.impl;

import com.competitions.demo.app.entities.Competition;
import com.competitions.demo.app.exceptions.CompetitionAlreadyExistsException;
import com.competitions.demo.app.exceptions.CompetitionNotFoundException;
import com.competitions.demo.app.repositories.CompetitionRepository;
import com.competitions.demo.app.services.CompetitionService;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Log
public class CompetitionServiceImpl implements CompetitionService {

    private final CompetitionRepository competitionRepository;

    public CompetitionServiceImpl(CompetitionRepository competitionRepository) {
        this.competitionRepository = competitionRepository;
    }

    @Override
    public List<Competition> findAll() {
        return competitionRepository.findAll();
    }

    @Override
    public void create(Competition competition) {
        if (isExists(competition.getId())) {
            throw new CompetitionAlreadyExistsException(competition.getId());
        }
        competitionRepository.save(competition);
    }

    @Override
    public Competition findById(String id) throws CompetitionNotFoundException {
        Optional<Competition> competition = competitionRepository.findById(id);

        return competition.orElseThrow(()-> new CompetitionAlreadyExistsException(String.format("competition with id %s not found", id)));
    }

    @Override
    public boolean isExists(String id) {
        return competitionRepository.existsById(id);
    }
}
