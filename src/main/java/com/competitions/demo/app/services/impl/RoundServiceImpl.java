package com.competitions.demo.app.services.impl;

import com.competitions.demo.app.entities.Record;
import com.competitions.demo.app.entities.Round;
import com.competitions.demo.app.exceptions.InvalidRoundNumException;
import com.competitions.demo.app.repositories.RoundRepository;
import com.competitions.demo.app.services.RoundService;
import com.competitions.demo.util.ScrambleGenerator;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoundServiceImpl implements RoundService  {
    private final RoundRepository roundRepository;
    private final ScrambleGenerator scrambleGenerator;
    private final int ALGORITHM_LENGTH = 25;

    public RoundServiceImpl(
            RoundRepository roundRepository,
            ScrambleGenerator scrambleGenerator) {
        this.roundRepository = roundRepository;
        this.scrambleGenerator = scrambleGenerator;
    }

    @Override
    public Round findByCompetitionUserRound(String comp_id, String username, Integer round_num) {
        if (round_num < 1 || round_num > 5) {
            throw new InvalidRoundNumException("Round num 1-5 expected");
        }
        return roundRepository.findByRecord_Competition_IdAndRecord_User_UsernameAndRoundNum(comp_id, username, round_num);
    }

    @Override
    public List<Round> findByRecord(Record record) {
        return roundRepository.findAllByRecord(record);
    }

    @Override
    public void create(Round round) {
        round.setScramble(scrambleGenerator.getScramble(ALGORITHM_LENGTH));
        roundRepository.save(round);
    }

    @Override
    public void update(Round round) {
        roundRepository.save(round);
    }
}
