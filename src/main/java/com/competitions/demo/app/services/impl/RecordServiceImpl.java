package com.competitions.demo.app.services.impl;

import com.competitions.demo.app.dto.RecordDTO;
import com.competitions.demo.app.dto.RoundResultsDTO;
import com.competitions.demo.app.entities.Competition;
import com.competitions.demo.app.entities.Record;
import com.competitions.demo.app.entities.Round;
import com.competitions.demo.app.entities.User;
import com.competitions.demo.app.exceptions.*;
import com.competitions.demo.app.repositories.RecordRepository;
import com.competitions.demo.app.services.CompetitionService;
import com.competitions.demo.app.services.RecordService;
import com.competitions.demo.app.services.RoundService;
import com.competitions.demo.app.services.UserService;
import lombok.extern.java.Log;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@Log
public class RecordServiceImpl implements RecordService {
    private final RecordRepository recordRepository;
    private final RoundService roundService;
    private final UserService userService;
    private final CompetitionService competitionService;

    public RecordServiceImpl(
            RecordRepository recordRepository,
            RoundService roundService,
            UserService userService,
            CompetitionService competitionService) {
        this.recordRepository = recordRepository;
        this.roundService = roundService;
        this.userService = userService;
        this.competitionService = competitionService;
    }

    @Override
    public void create(Competition competition, User user) {
        Record record = new Record();
        record.setCompetition(competition);
        record.setUser(user);
        recordRepository.save(record);
        for (int i = 1; i <= 5; i++) {
            Round round = new Round();
            round.setRoundNum(i);
            round.setRecord(record);
            roundService.create(round);
        }
    }

    @Override
    public List<Record> findAllByCompetitionId(String id) {
        if (!competitionService.isExists(id)) {
            log.info("competition not found");
            throw new CompetitionNotFoundException(id);
        }
        log.info("competition exists");
        List<Record> records = recordRepository.findAllByCompetition_Id(id);
        Collections.sort(records);
        return records;
    }

    @Override
    public Record findByCompetitionAndUser(String comp_id, String username) {
        competitionService.findById(comp_id);
        if (!userService.isExists(username)) {
            throw new UsernameNotFoundException(username + " Not Found");
        }
        Record record = recordRepository.findByCompetition_IdAndUser_Username(comp_id, username);
        if (record == null) {
            throw new UserNotRegisteredForCompetitionException(username, comp_id);
        }
        return record;
    }

    @Override
    public List<RecordDTO> findAllByUsername(String username) {
        List<RecordDTO> result = new ArrayList<>();
        for (Record record: recordRepository.findAllByUser_Username(username)) {
            result.add(new RecordDTO(record));
        }
        return result;
    }

    @Override
    public void registerForCompetition(String comp_id, String username) {
        if (isExists(comp_id, username)) {
            throw new UserAlreadyRegisteredException(username + " already registered for " + comp_id);
        }
        Competition competition = competitionService.findById(comp_id);
        User user = userService.findByUsername(username);
        create(competition, user);
    }

    @Override
    public boolean isExists(String comp_id, String username) {
        return recordRepository.existsByCompetition_IdAndUser_Username(comp_id, username);
    }

    @Override
    public void setRound(String comp_id, String username, int round_num, RoundResultsDTO roundResultsDTO) {
        log.info("setting Round: " + roundResultsDTO.getTime());
        if (!userService.isExists(username)) {
            log.info("1");
            throw new UserNotFoundException(username);
        }
        if (!competitionService.isExists(comp_id)) {
            log.info("2");
            throw new CompetitionNotFoundException(comp_id);
        }
        if (!isExists(comp_id, username)) {
            log.info("3");
            throw new RecordNotFoundException(comp_id, username);
        }

        Round round = roundService.findByCompetitionUserRound(comp_id, username, round_num);
        Record record = round.getRecord();
        User user = record.getUser();

        int roundsComplete = record.getRounds_complete();
        log.info(String.format("%s %s", roundsComplete, round.getRoundNum()));

        if (roundsComplete + 1 != round.getRoundNum()) {
            throw new InvalidRoundNumException(
                    String.format("Currently only round: %s available", roundsComplete));
        }
        record.setRounds_complete(roundsComplete + 1);

        round.setTime(roundResultsDTO.getTime());
        round.setTaken(true);

        if (record.getBest_time() == null || record.getBest_time() < round.getTime()) {
            record.setBest_time(round.getTime());
            user.setBestSingle(round.getTime());
        }

        if (record.getWorst_time() == null || record.getWorst_time() > round.getTime()) {
            record.setWorst_time(round.getTime());
        }

        if (record.getRounds_complete() == 5) {
            List<Round> rounds = roundService.findByRecord(record);
            rounds.sort(
                    (Round o1, Round o2) -> {
                        if (o1.getTime() > o2.getTime()) {
                            return 1;
                        }
                        if (o1.getTime().equals(o2.getTime())) {
                            return 0;
                        }
                        return -1;
                    }
            );
            Double sum = 0.0;
            for (int i = 1; i <= 3; i++) {
                sum += rounds.get(i).getTime();
            }
            record.setAverage(sum/3);
            if (user.getBestAvg() == null || record.getAverage() > user.getBestAvg()) {
                user.setBestAvg(record.getAverage());
            }
        }
        userService.update(user);
        recordRepository.save(record);
        roundService.update(round);
    }

}
