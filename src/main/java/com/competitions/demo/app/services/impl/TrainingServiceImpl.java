package com.competitions.demo.app.services.impl;

import com.competitions.demo.app.dto.TrainResultDTO;
import com.competitions.demo.app.entities.Training;
import com.competitions.demo.app.entities.User;
import com.competitions.demo.app.exceptions.InvalidTrainingLevelException;
import com.competitions.demo.app.exceptions.UserNotFoundException;
import com.competitions.demo.app.repositories.TrainingRepository;
import com.competitions.demo.app.services.TrainingService;
import com.competitions.demo.app.services.UserService;
import com.competitions.demo.util.ScrambleGenerator;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TrainingServiceImpl implements TrainingService {

    private final TrainingRepository trainingRepository;
    private final ScrambleGenerator scrambleGenerator;
    private final UserService userService;

    public TrainingServiceImpl(
            TrainingRepository trainingRepository,
            UserService userService,
            ScrambleGenerator scrambleGenerator) {
        this.trainingRepository = trainingRepository;
        this.userService = userService;
        this.scrambleGenerator = scrambleGenerator;
    }

    @Override
    public String getScrambleByLevel(int level) {
        User user = userService.getTokenUser();
        if (user.getTrainLevel() < level) {
            throw new InvalidTrainingLevelException(
                    String.format("maximum %s level is available", user.getTrainLevel()));
        }
        return scrambleGenerator.getScramble(level+1);
    }

    @Override
    public boolean setTrainResults(TrainResultDTO trainResultDTO) {
        User user = userService.getTokenUser();
        int level = trainResultDTO.getLevel();

        if (user.getTrainLevel() < level) {
            throw new InvalidTrainingLevelException(
                    String.format("maximum %s level is available", user.getTrainLevel()));
        }

        Training training = trainingRepository.findByUser_UsernameAndLevel(user.getUsername(), level);
        if (training == null) {
            training = new Training();
            training.setUser(user);
            training.setLevel(level);
            user.setTrainLevel(level+1);
        }
        if (training.getTime() == null || training.getTime() > trainResultDTO.getTime()) {
            training.setTime(trainResultDTO.getTime());
            trainingRepository.save(training);
            return true;
        }
        userService.update(user);
        trainingRepository.save(training);
        return false;
    }

    @Override
    public List<Training> getUserTrainings(String username) {
        if (!userService.isExists(username)) {
            throw new UserNotFoundException(username);
        }
        return trainingRepository.findAllByUser_Username(username);
    }
}
