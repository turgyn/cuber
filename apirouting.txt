** do not requires authentication

     /api/v1

* POST    /register
* POST    /login
* GET     /users                              list of all users
* GET     /users/{username}                   detailed info [username]
* GET     /users/{username}/records           all participation of [username]
* GET     /competitions                     list of competitions
* GET     /competitions/{id}                  competition results
* GET     /competitions/{id}/{username}           competition results for [username]
* GET     /competitions/{id}/{username}/{round}    information about round
GET     /training/{username}                    list of training records

##    ?order-by=[single/avg/name]?limit=[int]   фильтры для результатов соревнований и общего рейтинга
-------------------------------------------------------------------------------------------------
** ROLE_USER

* GET     /profile            current session user details
* POST    /profile            update profile

* POST    /competition/{id}/register               register for competition
GET     /training                                get next level challenge
POST    /training                                insert results of last challenge
-------------------------------------------------------------------------------------------------
** ROLE_ADMIN

* POST    /competitions                                   create new competition

* GET     /competitions/{id}/{username}/{round_num}     get round info & scramble (algorithm for cube)
* POST    /competitions/{id}/{username}/{round_num}     insert time taken for solving cube